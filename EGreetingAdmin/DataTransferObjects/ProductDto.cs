﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EGreetingAdmin.DataTransferObjects
{
    public class ProductDto
    {
        [Required]
        [JsonProperty(propertyName: "name")]
        public string Name { get; set; }
        [Required]
        [JsonProperty(propertyName: "url")]
        public string Url { get; set; }
        [Required]
        [JsonProperty(propertyName: "description")]
        public string Description { get; set; }
        [Required]
        [JsonProperty(propertyName: "video")]
        public string Video { get; set; }
        [Required]
        [JsonProperty(propertyName: "image")]
        public string Image { get; set; }
        [Required]
        [JsonProperty(propertyName: "category")]
        public string Category { get; set; }
    }
}