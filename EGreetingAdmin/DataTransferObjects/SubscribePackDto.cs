﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EGreetingAdmin.DataTransferObjects
{
    public class SubscribePackDto
    {
        [Required]
        [JsonProperty(propertyName: "name")]
        public string Name { get; set; }
        [Required]
        [JsonProperty(propertyName: "effective_long")]
        public int EffectiveLong { get; set; }
        [Required]
        [JsonProperty(propertyName: "price")]
        public int Price { get; set; }
    }
}
