﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EGreetingAdmin.DataTransferObjects
{
    public class UserDto
    {
        [JsonProperty(propertyName: "username")]
        [Required]
        public string Username { get; set; }
        [JsonProperty(propertyName: "password")]
        [Required]
        public string Password { get; set; }
    }
}
