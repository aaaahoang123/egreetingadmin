﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using EGreetingAdmin.Models;
using EGreetingAdmin.Resource;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace EGreetingAdmin.Helper
{
    public class HttpHelper
    {
        public static async Task<ApiResponse<T>> GetJson<T>(string url, HttpRequest req)
        {
            using (HttpClient client = new HttpClient())
            {
                // Call asynchronous network methods in a try/catch block to handle exceptions
                client.DefaultRequestHeaders.Add("authorization", "Bearer " + req.Cookies["access_token"]);
                string response = await client.GetStringAsync(url);

                ApiResponse<T> res = JsonConvert.DeserializeObject<ApiResponse<T>>(response);

                return res;

                // Above three lines can be replaced with new helper method below
                // string responseBody = await client.GetStringAsync(uri);
            }
        }

        public static async Task<ApiResponse<T>> PostJson<T>(string url, T obj, HttpRequest req)
        {
            using (HttpClient client = new HttpClient())
            {
                // Call asynchronous network methods in a try/catch block to handle exceptions

                client.DefaultRequestHeaders.Add("authorization", "Bearer " + req.Cookies["access_token"]);
                HttpResponseMessage response =
                    await client.PostAsJsonAsync(url, obj);

                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<ApiResponse<T>>(responseBody);
            }
        }

        public static async Task<ApiResponse<T>> PutJson<T>(string url, T obj, HttpRequest req)
        {
            using (HttpClient client = new HttpClient())
            {
                // Call asynchronous network methods in a try/catch block to handle exceptions

                client.DefaultRequestHeaders.Add("authorization", "Bearer " + req.Cookies["access_token"]);
                HttpResponseMessage response =
                    await client.PutAsJsonAsync(url, obj);

                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<ApiResponse<T>>(responseBody);
            }
        }
    }
}