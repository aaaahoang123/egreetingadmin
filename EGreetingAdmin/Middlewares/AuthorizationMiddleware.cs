﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using EGreetingAdmin.Models;
using EGreetingAdmin.Resource;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace EGreetingAdmin.Middlewares
{
    public class AuthorizationMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IMemoryCache _memoryCache;
        private readonly ILogger<AuthorizationMiddleware> _logger;

        public AuthorizationMiddleware(RequestDelegate next, IMemoryCache memoryCache, ILogger<AuthorizationMiddleware> logger)
        {
            _next = next;
            _memoryCache = memoryCache;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Path.ToString().Contains("/authentication"))
            {
                await _next.Invoke(context);
                return;
            }

            var token = context.Request.Cookies["access_token"];
            if (token == null)
            {
                context.Response.Redirect("/authentication");
                return;
            }
            if (!_memoryCache.TryGetValue(token, out User user))
            {
                using (HttpClient client = new HttpClient())
                {
                    try
                    {
                        client.DefaultRequestHeaders.Add("authorization", "Bearer " + token);
                        HttpResponseMessage response = await client.GetAsync(StaticResource.AuthUrl + "user-data");
                        response.EnsureSuccessStatusCode();
                        string responseBody = await response.Content.ReadAsStringAsync();

                        var res = JsonConvert.DeserializeObject<ApiResponse<User>>(responseBody, StaticResource.JsonSerializerSettings);
                        user = res.Data;
                        var cacheEntryOption = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(StaticResource.CacheExpire));
                        _memoryCache.Set(token, user, cacheEntryOption);
                    }
                    catch (HttpRequestException e)
                    {
                        _logger.LogError(e.Message);
                        context.Response.Redirect("/authentication");
                        return;
                    }
                }
            }
            await _next.Invoke(context);
        }
       
    }

    public static class SecurityMiddlewareExtensions
    {
        public static IApplicationBuilder UseAuthorizationMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AuthorizationMiddleware>();
        }
    }
}
