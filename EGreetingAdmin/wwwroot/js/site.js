﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
//var BASE_URL = 'http://localhost:3000/';
var BASE_URL = 'https://e-greeting.herokuapp.com/';
var ADMIN_URL = BASE_URL + 'admin/';
var controller;

var CreateProductController = (function() {
    var controller = function() {
        $('#upload-image').on('change',
            function (e) {
                var form = new FormData();
                form.append('image', e.target.files[0]);
                $.ajax({
                    url: ADMIN_URL + 'upload-image',
                    method: 'post',
                    data: form,
                    contentType: false,
                    processData: false,
                    headers: {
                        authorization: 'Bearer ' + Cookies.get('access_token')
                    },
                    success: function(res) {
                        $('#demo-image').attr('src', res.data.image_url);
                        $('input[name="Image"]').val(res.data.image_url);
                    },
                    error: console.log
                });
            });

        $('#upload-video').on('change',
            function (e) {
                var form = new FormData();
                form.append('video', e.target.files[0]);
                $.ajax({
                    url: ADMIN_URL + 'upload-video',
                    method: 'post',
                    data: form,
                    contentType: false,
                    processData: false,
                    headers: {
                        authorization: 'Bearer ' + Cookies.get('access_token')
                    },
                    success: function(res) {
                        $('#demo-video').attr('src', res.data.video_url);
                        document.getElementById('demo-video').play();
                        $('input[name="Video"]').val(res.data.video_url);
                    },
                    error: console.log
                });
            });
    };

    return controller;
})();

var IndexController = (function() {
    var controller = function() {
        this.loadData();
        var self = this;
        $(document).ready(function() {
            $('#selectDate').on('change',
                function () {
                    console.log(jQuery(this).val());
                    self.loadData(jQuery(this).val());
                });
        });
    };

    controller.prototype.loadData = function (param) {
        var url = ADMIN_URL + 'dashboard-data';
        if (param)
            url += '?date=' + param;
        $.ajax({
            url: url,
            method: 'get',
            headers: {
                authorization: 'Bearer ' + Cookies.get('access_token')
            },
            success: function (res) {
                console.log(res);
                if (res.data.transactions.length > 0) {
                   
                    var total_transaction = 0;
                    var revenue = 0;
                    ['unpaid-transaction', 'paid-transaction', 'reject-transaction', 'total-transaction', 'process-schedule', 'done-schedule', 'total-schedule']
                        .forEach(function(id) {
                            $('#' + id).text(0);
                        });
                    for (var transaction of res.data.transactions[0].quantity_revenue) {
                        switch (transaction._id) {
                            case 0:
                                $('#unpaid-transaction').text(transaction.qty);
                                total_transaction += transaction.qty;
                                break;
                            case 1:
                                $('#paid-transaction').text(transaction.qty);
                                total_transaction += transaction.qty;
                                revenue += transaction.revenue;
                                break;
                            case -1:
                                $('#reject-transaction').text(transaction.qty);
                                total_transaction += transaction.qty;
                                break;
                        }
                    }
                    $('#total-transaction').text(total_transaction);
                }

                if (res.data.schedule.length > 0) {
                    var total_schedule = 0;
                    for (var schedule of res.data.schedule[0].quantity) {
                        switch (schedule._id) {
                            case 0:
                                total_schedule += schedule.qty;
                                $('#process-schedule').text(schedule.qty);
                                break;
                            case 1:
                                total_schedule += schedule.qty;
                                $('#done-schedule').text(schedule.qty);
                                break;
                        }
                    }

                    $('#total-schedule').text(total_schedule);
                }
            },
            error: console.log
        });
    };

    return controller;
})();

switch (location.pathname.toLocaleLowerCase()) {
    case '/products/create':
        controller = new CreateProductController();
        break;
    case '/':
        controller = new IndexController();
        break;
    default:
        break;
}