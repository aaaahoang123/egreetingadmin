﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EGreetingAdmin.Resource
{
    public class StaticResource
    {
        public const string BaseUrl = "https://e-greeting.herokuapp.com/";
//        public const string BaseUrl = "http://localhost:3000/";
        public const string ApiUrl = BaseUrl + "api/";
        public const string AdminUrl = BaseUrl + "admin/";
        public const string AuthUrl = BaseUrl + "auth/";
        public const int CookieExpire = 168;
        public const int CacheExpire = 5;
        public static JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore
        };
}
}
