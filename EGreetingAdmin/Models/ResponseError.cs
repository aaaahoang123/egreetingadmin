﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace EGreetingAdmin.Models
{
    public class ResponseError
    {
        [JsonProperty(propertyName: "status")]
        public int Status { get; set; }
        [JsonProperty(propertyName: "message")]
        public string Message { get; set; }
        [JsonProperty(propertyName: "errors")]
        public List<JObject> Errors { get; set; } 
    }
}
