﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EGreetingAdmin.Models
{
    public class User
    {
        [JsonProperty(propertyName: "_id")]
        [Key]
        public string Id { get; set; }
        [JsonProperty(propertyName: "username")]
        public string Username { get; set; }
        [JsonProperty(propertyName: "email")]
        public string Email { get; set; }
        [JsonProperty(propertyName: "phone")]
        public string Phone { get; set; }
        [JsonProperty(propertyName: "gender")]
        public int Gender { get; set; }
        [JsonProperty(propertyName: "birthday")]
        public string Birthday { get; set; }
        [JsonProperty(propertyName: "policy")]
        public int Policy { get; set; }
        [JsonProperty(propertyName: "created_at")]
        public DateTime CreatedAt { get; set; }
        [JsonProperty(propertyName: "updated_at")]
        public DateTime UpdatedAt { get; set; }
        [JsonProperty(propertyName: "status")]
        public int Status { get; set; }
        [JsonProperty(propertyName: "access_token")]
        public string AccessToken { get; set; }
    }
}
