﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace EGreetingAdmin.Models
{
    public class ApiResponse<T>
    {
        [JsonProperty(propertyName: "status")]
        public int Status { get; set; }
        [JsonProperty(propertyName: "message")]
        public string Message { get; set; }
        [JsonProperty(propertyName: "data")]
        public T Data { get; set; }
        [JsonProperty(propertyName: "datas")]
        public List<T> Datas { get; set; }
    }
}
