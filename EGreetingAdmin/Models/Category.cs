﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EGreetingAdmin.Models
{
    public class Category
    {
        [JsonProperty(propertyName: "_id")]
        [Key]
        public string Id { get; set; }
        [JsonProperty(propertyName: "name")]
        public string Name { get; set; }
        [JsonProperty(propertyName: "url")]
        public string Url { get; set; }
        [JsonProperty(propertyName: "description")]
        public string Description { get; set; }
        [JsonProperty(propertyName: "level")]
        public int Level { get; set; }
        [JsonProperty(propertyName: "created_at")]
        public DateTime CreatedAt { get; set; }
        [JsonProperty(propertyName: "updated_at")]
        public DateTime UpdatedAt { get; set; }
        [JsonProperty(propertyName: "status")]
        public int Status { get; set; }
        [JsonProperty(propertyName: "status_title")]
        public string StatusTitle { get; set; }
    }
}
