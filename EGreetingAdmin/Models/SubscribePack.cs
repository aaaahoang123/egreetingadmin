﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EGreetingAdmin.Models
{
    public class SubscribePack
    {
        [JsonProperty(propertyName: "_id")]
        [Key]
        public string Id { get; set; }

        [Required]
        [JsonProperty(propertyName: "name")]
        public string Name { get; set; }

        [Required]
        [JsonProperty(propertyName: "effective_long")]
        public int EffectiveLong { get; set; }

        [Required]
        [JsonProperty(propertyName: "price")]
        public int Price { get; set; }

        [JsonProperty(propertyName: "created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty(propertyName: "updated_at")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty(propertyName: "status")] public int Status { get; set; }
    }
}