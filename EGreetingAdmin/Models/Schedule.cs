﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EGreetingAdmin.Models
{
    public class Schedule
    {
        [JsonProperty(propertyName: "_id")]
        [Key]
        public string Id { get; set; }
        [JsonProperty(propertyName: "user_id")]
        [Required]
        public string UserId { get; set; }
        [JsonProperty(propertyName: "content")]
        [Required]
        public string Content { get; set; }
        [JsonProperty(propertyName: "send_time")]
        [Required]
        public string SendTime { get; set; }
        [JsonProperty(propertyName: "to_emails")]
        [NotMapped]
        public List<string> ToEmails { get; set; }
        [JsonProperty(propertyName: "created_at")]
        public string CreatedAt { get; set; }
        [JsonProperty(propertyName: "updated_at")]
        public string UpdatedAt { get; set; }
        [JsonProperty(propertyName: "status")]
        public int Status { get; set; }
        [JsonProperty(propertyName: "status_title")]
        public string StatusTitle { get; set; }
        [JsonProperty(propertyName: "user")]
        public User User { get; set; }
    }
}
