﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EGreetingAdmin.Models
{
    public class Product
    {
        [JsonProperty(propertyName: "_id")]
        [Key]
        public string Id { get; set; }
        [JsonProperty(propertyName: "name")]
        [Required]
        public string Name { get; set; }
        [JsonProperty(propertyName: "url")]
        [Required]
        public string Url { get; set; }
        [JsonProperty(propertyName: "description")]
        [Required]
        public string Description { get; set; }
        [JsonProperty(propertyName: "video")]
        [Required]
        public string Video { get; set; }
        [Required]
        [JsonProperty(propertyName: "image")]
        public string Image { get; set; }
        [JsonProperty(propertyName: "created_at")]
        public DateTime CreatedAt { get; set; }
        [JsonProperty(propertyName: "updated_at")]
        public DateTime UpdatedAt { get; set; }
        [JsonProperty(propertyName: "status")]
        public int Status { get; set; }
        [JsonProperty(propertyName: "status_title")]
        public string StatusTitle { get; set; }
        [JsonProperty(propertyName: "category_id")]
        public string CategoryId { get; set; }
        [JsonProperty(propertyName: "category")]
        public Category Category { get; set; }
    }
}
