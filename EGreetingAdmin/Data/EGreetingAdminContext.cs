﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using EGreetingAdmin.Models;

namespace EGreetingAdmin.Models
{
    public class EGreetingAdminContext : DbContext
    {
        public EGreetingAdminContext (DbContextOptions<EGreetingAdminContext> options)
            : base(options)
        {
        }

        public DbSet<EGreetingAdmin.Models.User> User { get; set; }

        public DbSet<EGreetingAdmin.Models.Category> Category { get; set; }

        public DbSet<EGreetingAdmin.Models.Product> Product { get; set; }

        public DbSet<EGreetingAdmin.Models.SubscribePack> SubscribePack { get; set; }

        public DbSet<EGreetingAdmin.Models.Schedule> Schedule { get; set; }

        public DbSet<EGreetingAdmin.Models.Transaction> Transaction { get; set; }
    }
}
