﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EGreetingAdmin.Helper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EGreetingAdmin.Models;
using EGreetingAdmin.Resource;

namespace EGreetingAdmin.Controllers
{
    public class TransactionsController : Controller
    {
        private readonly EGreetingAdminContext _context;

        public TransactionsController(EGreetingAdminContext context)
        {
            _context = context;
        }

        // GET: Transactions
        public async Task<IActionResult> Index()
        {
            var transactions = (await HttpHelper.GetJson<Transaction>(StaticResource.AdminUrl + "transactions", Request)).Datas;
            return View(transactions);
        }

        // GET: Transactions/Edit/5/paid
        public async Task<IActionResult> Edit(string id, string type)
        {
            if (id == null)
            {
                return NotFound();
            }

            await HttpHelper.PutJson(StaticResource.AdminUrl + "transactions?id=" + id + "&type=" + type, new {}, Request);
           
            return RedirectToAction("Index");
        }

        // POST: Transactions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,UserId,UnitPrice,CreatedAt,UpdatedAt,Status,StatusTitle")] Transaction transaction)
        {
            if (id != transaction.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(transaction);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TransactionExists(transaction.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserId"] = new SelectList(_context.User, "Id", "Id", transaction.UserId);
            return View(transaction);
        }

        // GET: Transactions/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var transaction = await _context.Transaction
                .Include(t => t.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (transaction == null)
            {
                return NotFound();
            }

            return View(transaction);
        }

        // POST: Transactions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var transaction = await _context.Transaction.FindAsync(id);
            _context.Transaction.Remove(transaction);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TransactionExists(string id)
        {
            return _context.Transaction.Any(e => e.Id == id);
        }
    }
}
