﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using EGreetingAdmin.DataTransferObjects;
using EGreetingAdmin.Helper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EGreetingAdmin.Models;
using EGreetingAdmin.Resource;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace EGreetingAdmin.Controllers
{
    public class CategoriesController : Controller
    {
        private readonly EGreetingAdminContext _context;

        public CategoriesController(EGreetingAdminContext context)
        {
            _context = context;
        }

        // GET: Categories
        public async Task<IActionResult> Index()
        {
            using (HttpClient client = new HttpClient())
            {
                // Call asynchronous network methods in a try/catch block to handle exceptions
                try
                {
                    client.DefaultRequestHeaders.Add("authorization", "Bearer " + Request.Cookies["access_token"]);
                    string response = await client.GetStringAsync(StaticResource.ApiUrl + "categories?all=1");

                    List<Category> list = JsonConvert.DeserializeObject<ApiResponse<Category>>(response).Datas;

                    return View(list);

                    // Above three lines can be replaced with new helper method below
                    // string responseBody = await client.GetStringAsync(uri);
                }
                catch (HttpRequestException e)
                {
                    return Json(new
                    {
                        Error = e
                    });
                }
            }
        }

        // GET: Categories/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            using (HttpClient client = new HttpClient())
            {
                // Call asynchronous network methods in a try/catch block to handle exceptions
                try
                {
                    client.DefaultRequestHeaders.Add("authorization", "Bearer " + Request.Cookies["access_token"]);
                    string response = await client.GetStringAsync(StaticResource.ApiUrl + "categories/" + id);

                    Category category = JsonConvert.DeserializeObject<ApiResponse<Category>>(response).Data;
                    if (category == null)
                    {
                        return NotFound();
                    }
                    return View(category);

                    // Above three lines can be replaced with new helper method below
                    // string responseBody = await client.GetStringAsync(uri);
                }
                catch (HttpRequestException e)
                {
                    return Json(new
                    {
                        Error = e
                    });
                }
            }
        }

        // GET: Categories/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Url,Description,Level")] CategoryDto categoryDto)
        {
            if (ModelState.IsValid)
            {
                using (HttpClient client = new HttpClient())
                {
                    // Call asynchronous network methods in a try/catch block to handle exceptions
                    try
                    {
                        client.DefaultRequestHeaders.Add("authorization", "Bearer " + Request.Cookies["access_token"]);
                        HttpResponseMessage response = await client.PostAsJsonAsync(StaticResource.AdminUrl + "categories", categoryDto);

                        string responseBody = await response.Content.ReadAsStringAsync();
                        if (response.IsSuccessStatusCode)
                        {
                            return RedirectToAction("Index");
                        }

                        TempData["old_category"] = JsonConvert.SerializeObject(categoryDto);
                        return RedirectToAction("Create");

                        // Above three lines can be replaced with new helper method below
                        // string responseBody = await client.GetStringAsync(uri);


                    }
                    catch (HttpRequestException e)
                    {
                        return Json(new
                        {
                            Data = e
                        });
                    }
                }
            }
            return View(categoryDto);
        }

        // GET: Categories/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            using (HttpClient client = new HttpClient())
            {
                // Call asynchronous network methods in a try/catch block to handle exceptions
                try
                {
                    client.DefaultRequestHeaders.Add("authorization", "Bearer " + Request.Cookies["access_token"]);
                    string response = await client.GetStringAsync(StaticResource.ApiUrl + "categories/" + id);

                    Category category = JsonConvert.DeserializeObject<ApiResponse<Category>>(response).Data;
                    if (category == null)
                    {
                        return NotFound();
                    }
                    return View(category);

                    // Above three lines can be replaced with new helper method below
                    // string responseBody = await client.GetStringAsync(uri);
                }
                catch (HttpRequestException e)
                {
                    return Json(new
                    {
                        Error = e
                    });
                }
            }
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Name,Url,Description,Level,CreatedAt,UpdatedAt,Status")] Category category)
        {
            if (ModelState.IsValid)
            {
                using (HttpClient client = new HttpClient())
                {
                    // Call asynchronous network methods in a try/catch block to handle exceptions
                    try
                    {
                        client.DefaultRequestHeaders.Add("authorization", "Bearer " + Request.Cookies["access_token"]);
                        HttpResponseMessage response = await client.PutAsJsonAsync(StaticResource.AdminUrl + "categories/" + id, category);

                        string responseBody = await response.Content.ReadAsStringAsync();
                        if (response.IsSuccessStatusCode)
                        {
                            return RedirectToAction("Index");
                        }

                        TempData["old_category"] = JsonConvert.SerializeObject(category);

                        // Above three lines can be replaced with new helper method below
                        // string responseBody = await client.GetStringAsync(uri);


                    }
                    catch (HttpRequestException e)
                    {
                        return Json(e);
                    }
                }
            }
            return View(category);
        }

        // GET: Categories/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var category = (await HttpHelper.GetJson<Category>(StaticResource.ApiUrl + "categories/" + id, Request))
                .Data;
            if (category == null)
            {
                return NotFound();
            }

            return View(category);
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string url)
        {
            await HttpHelper.GetJson<Category>(StaticResource.AdminUrl + "categories/" + url + "/delete", Request);
            return RedirectToAction(nameof(Index));
        }

        private bool CategoryExists(string id)
        {
            return _context.Category.Any(e => e.Id == id);
        }
    }
}
