﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EGreetingAdmin.DataTransferObjects;
using EGreetingAdmin.Helper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EGreetingAdmin.Models;
using EGreetingAdmin.Resource;

namespace EGreetingAdmin.Controllers
{
    public class ProductsController : Controller
    {
        private readonly EGreetingAdminContext _context;

        public ProductsController(EGreetingAdminContext context)
        {
            _context = context;
        }

        // GET: Products
        public async Task<IActionResult> Index()
        {
            List<Product> products = (await HttpHelper.GetJson<Product>(StaticResource.ApiUrl + "products", Request)).Datas;
            return View(products);
        }

        // GET: Products/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = (await HttpHelper.GetJson<Product>(StaticResource.ApiUrl + "products/" + id, Request)).Data;
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // GET: Products/Create
        public async Task<IActionResult> Create()
        {
            List<Category> categories =
                (await HttpHelper.GetJson<Category>(StaticResource.ApiUrl + "categories?all=1", Request)).Datas;
            ViewData["categories"] = categories;
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Url,Description,Video,Image,Category")] ProductDto product)
        {
            if (ModelState.IsValid)
            {
                await HttpHelper.PostJson(StaticResource.AdminUrl + "products", product, Request);
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Products/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productTask = HttpHelper.GetJson<Product>(StaticResource.ApiUrl + "products/" + id, Request);
            var categoriesTask = HttpHelper.GetJson<Category>(StaticResource.ApiUrl + "categories?all=1", Request);
            var product = (await productTask).Data;
            if (product == null)
            {
                return NotFound();
            }

            var categories = (await categoriesTask).Datas;
            ViewData["categories"] = categories;
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,Name,Url,Description,Video,Image,Status,CategoryId")] Product product)
        {
            
                try
                {
                    await HttpHelper.PutJson(StaticResource.AdminUrl + "products/" + product.Url, product, Request);
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    return Json(e);
//                    return View(product);
                }
        }

        // GET: Products/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            var product =
                (await HttpHelper.GetJson<Product>(StaticResource.ApiUrl + "products/" + id, Request))
                .Data; 
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            await HttpHelper.GetJson<Product>(StaticResource.AdminUrl + "products/" + id + "/delete", Request);
            return RedirectToAction(nameof(Index));
        }

        private bool ProductExists(string id)
        {
            return _context.Product.Any(e => e.Id == id);
        }
    }
}
