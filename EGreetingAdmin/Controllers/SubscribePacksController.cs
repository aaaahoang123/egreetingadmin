﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EGreetingAdmin.DataTransferObjects;
using EGreetingAdmin.Helper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EGreetingAdmin.Models;
using EGreetingAdmin.Resource;

namespace EGreetingAdmin.Controllers
{
    public class SubscribePacksController : Controller
    {
        private readonly EGreetingAdminContext _context;

        public SubscribePacksController(EGreetingAdminContext context)
        {
            _context = context;
        }

        // GET: SubscribePacks
        public async Task<IActionResult> Index()
        {
            List<SubscribePack> list =
                (await HttpHelper.GetJson<SubscribePack>(StaticResource.ApiUrl + "subscribe-packs", Request)).Datas;
            return View(list);
        }

        // GET: SubscribePacks/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subscribePack =
                (await HttpHelper.GetJson<SubscribePack>(StaticResource.ApiUrl + "subscribe-packs/" + id, Request))
                .Data;
            if (subscribePack == null)
            {
                return NotFound();
            }

            return View(subscribePack);
        }

        // GET: SubscribePacks/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: SubscribePacks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,EffectiveLong,Price")] SubscribePackDto subscribePack)
        {
            if (ModelState.IsValid)
            {
                await HttpHelper.PostJson(StaticResource.AdminUrl + "subscribe-packs", subscribePack, Request);
                return RedirectToAction(nameof(Index));
            }
            return View(subscribePack);
        }

        // GET: SubscribePacks/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subscribePack =
                (await HttpHelper.GetJson<SubscribePack>(StaticResource.ApiUrl + "subscribe-packs/" + id, Request))
                .Data;
            if (subscribePack == null)
            {
                return NotFound();
            }
            return View(subscribePack);
        }

        // POST: SubscribePacks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,Name,EffectiveLong,Price,Status")] SubscribePack subscribePack)
        {
            if (id != subscribePack.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await HttpHelper.PutJson(StaticResource.AdminUrl + "subscribe-packs/" + id, subscribePack, Request);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SubscribePackExists(subscribePack.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(subscribePack);
        }

        // GET: SubscribePacks/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subscribePack = (await HttpHelper.GetJson<SubscribePack>(StaticResource.ApiUrl + "subscribe-packs/" + id, Request))
                .Data;
            if (subscribePack == null)
            {
                return NotFound();
            }

            return View(subscribePack);
        }

        // POST: SubscribePacks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            await HttpHelper.PostJson(StaticResource.AdminUrl + "subscribe-packs/" + id + "/delete", new SubscribePack(), Request);
            return RedirectToAction(nameof(Index));
        }

        private bool SubscribePackExists(string id)
        {
            return _context.SubscribePack.Any(e => e.Id == id);
        }
    }
}
